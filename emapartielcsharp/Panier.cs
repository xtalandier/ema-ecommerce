﻿using emapartielcsharp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace emapartielcsharp
{
    public static class Panier
    {
        private static List<Products> Produits = new List<Products>();
        public static void AddProduct(Products produit)
        {
            Produits.Add(produit);
        }

        public static decimal GetTotalPrice()
        {
            decimal? total = (from pds in Produits
                       select pds.Price).Sum();
            return total.HasValue ? total.Value : 0;
        }

        public static List<Products> GetProduits()
        {
            return Produits;
        }
    }
}