﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using wpf.Model;
using wpf.ViewModel;

namespace wpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private bool isEditing = false;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if(isEditing){
                try
                {
                    MainViewModel.mvm.Produits.Add(new Produit()
                    {
                        Nom = txtNom.Text,
                        Prix = double.Parse(txtPrix.Text),
                        Category = txtCat.Text
                    });
                }catch(Exception ex){
                    MessageBox.Show("Erreur...");
                }
            }
            DoubleAnimation GoAway = new DoubleAnimation();
            GoAway.AutoReverse = false;
            GoAway.To = 400;
            GoAway.Duration = new Duration(TimeSpan.FromSeconds(1));

            DoubleAnimation ComeHere = new DoubleAnimation();
            ComeHere.AutoReverse = false;
            ComeHere.To = 0;
            ComeHere.Duration = new Duration(TimeSpan.FromSeconds(1));

            ListTranslate.BeginAnimation(TranslateTransform.XProperty, isEditing ? ComeHere : GoAway, HandoffBehavior.SnapshotAndReplace);
            FormTranslate.BeginAnimation(TranslateTransform.XProperty, isEditing ? GoAway : ComeHere, HandoffBehavior.SnapshotAndReplace);
            Btn1.Content = isEditing ? "Ajouter" : "Enregistrer";
            isEditing = !isEditing;
        }

        private void BtnEnreg_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
