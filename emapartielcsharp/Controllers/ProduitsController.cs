﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using emapartielcsharp.Models;

namespace emapartielcsharp.Controllers
{
    public class ProduitsController : Controller
    {
        private Model1Container db = new Model1Container();

        //
        // GET: /Products/

        public ActionResult Index()
        {
            return View(db.Products.ToList());
        }

        //
        // GET: /Products/Details/5

        public ActionResult Details(int id = 0)
        {
            Products products = db.Products.Find(id);
            if (products == null)
            {
                return HttpNotFound();
            }
            AddAnalytic(products, "affichage");
            return View(products);
        }
        //
        // POST: /Products/Details/5

        [HttpPost]
        public ActionResult Details(int id = 0, int qtt = 1)
        {
            Products product = db.Products.Find(id);
            if (product == null)
            {
                return HttpNotFound();
            }
            Panier.AddProduct(product);
            AddAnalytic(product, "panier");
            return View(product);
        }

        private void AddAnalytic(Products p, string action)
        {
            if (action == "affichage")
            {
                p.CptAffichage++;
            }
            else
            {
                p.CptPanier++;
            }
            db.Entry(p).State = EntityState.Modified;
            db.SaveChanges();        }


        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}