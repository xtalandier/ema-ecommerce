﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace wpf.Model
{
    public class Produit
    {
        public string Nom { get; set; }
        public string Description { get; set; }
        public double Prix { get; set; }
        public string Category { get; set; }
    }

}
