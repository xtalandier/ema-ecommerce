﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace emapartielcsharp
{
    public class PanierFilterAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            filterContext.Controller.ViewBag.PanierPrix = Panier.GetTotalPrice();
            filterContext.Controller.ViewBag.PanierItems = Panier.GetProduits();
        }
    }
}