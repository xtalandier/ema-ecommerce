using GalaSoft.MvvmLight;
using System;
using System.Collections.ObjectModel;
using wpf.Model;

namespace wpf.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {

        public static MainViewModel mvm;
        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        /// 

        private ObservableCollection<Produit> _produits = new ObservableCollection<Produit>();

        public MainViewModel()
        {
            mvm = this;
            Random rnd = new Random();
            for (int i = 1; i < 10; i++)
            {
                Produits.Add(new Produit()
                {
                    Nom = "Produit #" + i,
                    Description = "Une description",
                    Prix = rnd.NextDouble() * 100,
                    Category = "Cat 1" + rnd.Next(1, 4)
                });
            }

            ////if (IsInDesignMode)
            ////{
            ////    // Code runs in Blend --> create design time data.
            ////}
            ////else
            ////{
            ////    // Code runs "for real"
            ////}
        }

        
        public ObservableCollection<Produit> Produits
        {
            get { return _produits; }
            set
            {
                if (_produits != value)
                {
                    _produits = value;
                    RaisePropertyChanged(() => Produits);
                }
            }
        }

        public string txtNom { get; set; }

    }
}